package com.personal.postgresql.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import com.personal.postgresql.constants.ConstantesRespuestas;
import com.personal.postgresql.dto.AvionDTO;
import com.personal.postgresql.entities.Avion;
import com.personal.postgresql.entities.DatoBiometrico;
import com.personal.postgresql.mappers.AvionMapper;
import com.personal.postgresql.repositories.AvionDao;
import com.personal.postgresql.responses.RespuestaGenerica;

@Service
public class AvionService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	private final AvionMapper avionMaper = new AvionMapper();
	
	@Autowired
	AvionDao avionDao;
	
	public @ResponseBody ResponseEntity<RespuestaGenerica> obtenerAvion(long idAvion){
		
		try {
			RespuestaGenerica respuestaGenerica;
			if(avionDao.existsById(idAvion)) {
				Avion avion = this.avionDao.findById(idAvion).get();
				AvionDTO  AvionRetornado = this.avionMaper.obtenerAvionDTO(avion);
				respuestaGenerica = new RespuestaGenerica(AvionRetornado, ConstantesRespuestas.EXITO, "");
				
				
			}else {
				
				respuestaGenerica = new RespuestaGenerica(ConstantesRespuestas.ERRROR, "Avion no existe.");
			}
		
		return new ResponseEntity<RespuestaGenerica>(respuestaGenerica, HttpStatus.OK);
		
		}catch(Exception e) {
			
			logger.error("Error se presentan problemas al cargar EL Avion ");
			e.printStackTrace();
			
			return new ResponseEntity<RespuestaGenerica>(new RespuestaGenerica(ConstantesRespuestas.ERRROR, ""),
					HttpStatus.BAD_REQUEST);
		}
		
	
	}
	
	public @ResponseBody ResponseEntity<RespuestaGenerica> actualizarAviones(AvionDTO avionDTO){
		RespuestaGenerica respuestaGenerica;
		try {
			if(avionDTO != null) {
				Avion avion= this.avionMaper.obtenerAvion(avionDTO);
				this.avionDao.save(avion);
				respuestaGenerica = new RespuestaGenerica(ConstantesRespuestas.EXITO, "");
			} else {
				respuestaGenerica = new RespuestaGenerica(ConstantesRespuestas.ERRROR, "No se puede crear un Avion.");
			}
			return new ResponseEntity<RespuestaGenerica>(respuestaGenerica, HttpStatus.OK);
		} catch(Exception e) {
			
			logger.error("Error se presentan problemas al crear la cuenta ");
			e.printStackTrace();
			
			return new ResponseEntity<RespuestaGenerica>(new RespuestaGenerica(ConstantesRespuestas.ERRROR, ""),
					HttpStatus.BAD_REQUEST);
		

		}
	
	
	
		}

}

