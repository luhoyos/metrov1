package com.personal.postgresql.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.personal.postgresql.entities.Avion;

@Repository
public interface AvionDao extends CrudRepository<Avion, Long> {

	
	
}
