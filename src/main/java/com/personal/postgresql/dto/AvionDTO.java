package com.personal.postgresql.dto;

import java.io.Serializable;

import java.util.Date;

import lombok.Data;

@Data
public class AvionDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private long idAvion;
	private double distanUser;
	private double altUser;
	private double distaNivMar;
	private double personParacaida;
	private Date fechaRegistro;
	
	
}
