package com.personal.postgresql.mappers;

import com.personal.postgresql.dto.AvionDTO;
import com.personal.postgresql.entities.Avion;

public class AvionMapper {
	
	
	public AvionDTO obtenerAvionDTO (Avion avionAcastear) {
		
		AvionDTO avionDTOAretornar = new AvionDTO();
		
		avionDTOAretornar.setIdAvion(avionAcastear.getIdAvion());
		avionDTOAretornar.setDistanUser(avionAcastear.getDistanUser());
		avionDTOAretornar.setAltUser(avionAcastear.getAltUser());
		avionDTOAretornar.setPersonParacaida(avionAcastear.getPersonParacaida());
		avionDTOAretornar.setFechaRegistro(avionAcastear.getFechaRegistro());
		
		return avionDTOAretornar;
		
		
		
	}
	
	public Avion obtenerAvion (AvionDTO avionDTOAcastear) {
		
		Avion avionAretornar = new Avion();
		
		avionAretornar.setIdAvion(avionDTOAcastear.getIdAvion());
		avionAretornar.setDistanUser(avionDTOAcastear.getDistanUser());
		avionAretornar.setAltUser(avionDTOAcastear.getAltUser());
		avionAretornar.setPersonParacaida(avionAretornar.getPersonParacaida());
		avionAretornar.setFechaRegistro(avionDTOAcastear.getFechaRegistro());
		return avionAretornar;
		
		
	}
	
	
	

}
