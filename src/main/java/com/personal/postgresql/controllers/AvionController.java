package com.personal.postgresql.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.personal.postgresql.constants.ConstantesApis;
import com.personal.postgresql.constants.ConstantesRespuestas;
import com.personal.postgresql.dto.AvionDTO;
import com.personal.postgresql.responses.RespuestaGenerica;
import com.personal.postgresql.services.AvionService;

@RestController
@RequestMapping (value = ConstantesApis.AVION_API)
public class AvionController {

	@Autowired
	AvionService avionService;
	
	@GetMapping(value = ConstantesApis.AVION_API_OBTENER, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody  ResponseEntity<RespuestaGenerica> obtenerAvion(
			@RequestParam long idAvion){
		
		return this.avionService.obtenerAvion(idAvion);	
	}
	
	@PostMapping(value = ConstantesApis.AVION_API_GUARDAR,  produces = MediaType.APPLICATION_JSON_VALUE )
	public @ResponseBody ResponseEntity<RespuestaGenerica> obtenerrAvion(
			@RequestBody AvionDTO avion){
		
		return this.avionService.actualizarAviones(avion);	
	}
			
			
	
}
